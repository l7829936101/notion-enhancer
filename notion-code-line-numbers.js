// ==UserScript==
// @name         notion-codeline
// @namespace    http://tampermonkey.net/
// @version      2024-06-11
// @description  try to take over the world!
// @author       You
// @match        https://www.notion.so/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=notion.so
// @run-at document-end
// @grant        none
// ==/UserScript==

(function () {
    const singleLined = false
    const codeBlockSelector = '.notion-code-block.line-numbers'
    const numbersClass = 'code-line-numbers'
    const $temp = document.createElement('span');


    const numberCodeBlock = ($codeBlock) => {

        let $numbers = $codeBlock.querySelector(`.${numbersClass}`);
        if (!$numbers) {
            $numbers = document.createElement('span');
            $numbers.className = numbersClass;
            $numbers.innerText = '1';
            $codeBlock.prepend($numbers);

            $temp.innerText = " ";
            $codeBlock.lastElementChild.appendChild($temp);
            $codeBlock.lineHeight = $temp.getBoundingClientRect().height;
            $temp.remove();
        }

        const lines = $codeBlock.lastElementChild.innerText.split(/\r\n|\r|\n/),
            wordWrap = $codeBlock.lastElementChild.style.wordBreak === 'break-all';
        if (lines[lines.length - 1] === '') lines.pop();

        let fill = ("" + lines.length).length;

        let lineNumbers = '';
        for (let i = 0; i < lines.length; i++) {
            for (let f = ("" + (i + 1)).length; f < fill; ++f) {
                lineNumbers += '0';
            }

            lineNumbers += `${i + 1}\n`;
            if (wordWrap) {
                $temp.innerText = lines[i];
                $codeBlock.lastElementChild.appendChild($temp); // 将 `$temp` 直接添加到 `$codeBlock`
                const height = $temp.getBoundingClientRect().height;
                $temp.remove();

                for (let j = 1; j < (height / $codeBlock.lineHeight - 1); j++) lineNumbers += ' \n';
            }
        }

        if (!singleLined && lines.length < 2) lineNumbers = '';
        $numbers.innerText = lineNumbers;
    }
    const numberAllCodeBlocks = () => {
        document.querySelectorAll(codeBlockSelector).forEach($codeBlock => {
            numberCodeBlock($codeBlock);
        });
    };
    const observeCodeBlocks = (mutationsList) => {
        for (let mutation of mutationsList) {
            if ([...mutation.addedNodes, ...mutation.removedNodes].includes($temp) ||
                mutation.target.classList.contains(numbersClass) ||
                [...mutation.addedNodes, ...mutation.removedNodes].some($node =>
                    $node?.classList?.contains(numbersClass))) {
                continue;
            }

            if (mutation.target.matches(`${codeBlockSelector}, ${codeBlockSelector} *`)) {
                let $codeBlock = mutation.target;
                while (!$codeBlock.matches(codeBlockSelector)) $codeBlock = $codeBlock.parentElement;
                numberCodeBlock($codeBlock);
            }
        }
    };
    numberAllCodeBlocks();
    const observer = new MutationObserver(observeCodeBlocks);
    observer.observe(document.body, {childList: true, subtree: true});

    const style = document.createElement('style');
    style.textContent = `
.line-numbers .notion-code-block
.notion-code-block.line-numbers {
position: static;

}
.code-line-numbers:not(:empty) + div {
    padding-left: 64px !important;
}

.code-line-numbers {
    position: absolute;
    left: 0;
    right: calc(100% - 50px);
    top: 34px;
    padding-right: 10px;

    font-size: 85%;
    font-family: 'Menlo', ui-monospace;
    text-align: right;
    color: rgba(128, 128, 128, 0.5);
    z-index: 1;
    overflow: hidden;
    pointer-events: none;
    border-right: solid;
}

.code-line-numbers:empty {
    display: none;
}

.code-line-numbers::before {
    content: '';
    position: absolute;
    top: 0;
    right: calc(100% - 52px);
    width: 2px;
    height: 100%;
    display: block;
}`
    document.head.appendChild(style);
})();