// ==UserScript==
// @name         Notion Icon
// @namespace    http://tampermonkey.net/
// @version      2024-06-11
// @description  try to take over the world!
// @author       You
// @match        https://www.notion.so/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=notion.so
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    const iconPanelSelector = `.notion-media-menu > .notion-scroller:nth-child(2)`;
    const uploadPictureSelector = '[style="margin-top: 20px; display: flex; flex-direction: column; align-items: stretch;"]';


    function copyToClipboard(event) {
        // 复制图标
        const url = event.currentTarget.querySelector('img').src;
        navigator.clipboard.writeText(url)
            .then(() => {
            console.log("Text copied to clipboard:", url);
        })
            .catch(err => {
            console.error("Failed to copy text:", url);
        });
    }

    function createIconButton(url) {
        const buttonDiv = document.createElement('div');
        buttonDiv.className = 'custom-icon-button';
        buttonDiv.setAttribute('role', 'button');
        buttonDiv.setAttribute('tabindex', '0');

        const img = document.createElement('img');
        img.src = url;
        img.alt = 'icon';

        buttonDiv.onclick = copyToClipboard;

        buttonDiv.appendChild(img);
        return buttonDiv;
    }

    function chunkArray(array, size) {
        const result = [];
        for (let i = 0; i < array.length; i += size) {
            result.push(array.slice(i, i + size));
        }
        return result;
    }

    function createIconPanel(iconUrls) {
        const groupedIconUrls = chunkArray(iconUrls, 12);

        const panelDiv = document.createElement('div');
        panelDiv.className = 'custom-icon-panel';

        groupedIconUrls.forEach((group) => {
            const groupDiv = document.createElement('div');
            groupDiv.style.display = 'flex';
            group.forEach((url) => {
                const iconButton = createIconButton(url);
                groupDiv.appendChild(iconButton);
            });
            panelDiv.appendChild(groupDiv);
        });

        return panelDiv;
    }

    const iconUrls = [
        "https://cdn.iconscout.com/icon/free/png-256/lens-zoom-glass-find-search-magnifier-10-16018.png",
        "https://cdn.iconscout.com/icon/free/png-256/lens-zoom-glass-find-search-magnifier-delete-remove-3-15973.png",
        "https://cdn.iconscout.com/icon/free/png-256/lens-zoom-glass-find-search-magnifier-9-16001.png",
        "https://cdn.iconscout.com/icon/free/png-256/lens-zoom-glass-find-search-magnifier-verify-perfect-3-15957.png",
        "https://cdn.iconscout.com/icon/free/png-256/lens-zoom-glass-find-search-magnifier-zoomin-add-3-15969.png",
        "https://cdn.iconscout.com/icon/free/png-512/screw-nut-tool-wrench-bolt-hardware-maintanence-10-15956.png",
        "https://cdn.iconscout.com/icon/free/png-256/screw-nut-tool-wrench-bolt-hardware-maintanence-11-15985.png",
        "https://cdn.iconscout.com/icon/free/png-256/screw-nut-tool-wrench-bolt-hardware-maintanence-9-15951.png",
        "https://cdn.iconscout.com/icon/free/png-256/heart-love-like-favorite-romance-gift-3-16027.png",
        "https://cdn.iconscout.com/icon/free/png-256/heart-favourite-favorite-love-like-outline-interface-13302.png",
        "https://cdn.iconscout.com/icon/free/png-256/check-verify-pure-write-round-tick-interface-ui-1-13373.png",
        "https://cdn.iconscout.com/icon/free/png-256/round-circle-grid-menu-option-11-16798.png",
        "https://cdn.iconscout.com/icon/free/png-512/round-circle-shape-design-disc-11-16794.png",
        "https://cdn.iconscout.com/icon/free/png-256/round-denied-remove-notice-circle-11-16749.png",
        "https://cdn.iconscout.com/icon/free/png-256/target-shoot-circle-mission-ui-fix-interface-13209.png",
        "https://cdn.iconscout.com/icon/free/png-512/target-keywords-seo-tools-search-find-dart-7008.png",
        "https://cdn.iconscout.com/icon/free/png-256/refresh-reload-retry-loading-try-interface-again-1-13186.png",
        "https://cdn.iconscout.com/icon/free/png-256/shutdown-shut-close-power-off-switchoff-13202.png",
        "https://cdn.iconscout.com/icon/free/png-256/tant-icecream-house-sand-3-15996.png",
        "https://cdn.iconscout.com/icon/free/png-256/tant-icecream-house-sand-11-16799.png",
        "https://cdn.iconscout.com/icon/free/png-256/artboard-type-cross-hairs-interface-ui-1-13347.png",
        "https://cdn.iconscout.com/icon/free/png-256/artboard-interface-ui-video-show-safe-areas-1-13346.png",
        "https://cdn.iconscout.com/icon/free/png-256/artboard-interface-ui-center-mark-centermark-show-1-13345.png",
        "https://cdn.iconscout.com/icon/free/png-256/horizontal-align-center-arrange-arrangement-tool-ui-13307.png",
        "https://cdn.iconscout.com/icon/free/png-256/horizontal-distribute-center-align-right-ui-tool-13310.png",
        "https://cdn.iconscout.com/icon/free/png-256/horizontal-align-left-ui-horizontally-tool-arrangements-13306.png",
        "https://cdn.iconscout.com/icon/free/png-256/horizontal-distribute-center-align-left-ui-horizontally-13308.png",
        "https://cdn.iconscout.com/icon/free/png-512/alarm-clock-ui-notification-reminder-time-1-13326.png",
        "https://cdn.iconscout.com/icon/free/png-256/calender-date-day-year-reminder-schedule-11-16782.png",
        "https://cdn.iconscout.com/icon/free/png-256/laptop-device-computer-screen-monitor-3-15972.png",
        "https://cdn.iconscout.com/icon/free/png-256/chat-message-notification-bubble-talk-3-15993.png",
        "https://cdn.iconscout.com/icon/free/png-256/chart-graph-analysis-statics-column-network-signal-3-15948.png",
        "https://cdn.iconscout.com/icon/free/png-256/setting-preferences-user-interface-ui-gear-3-15970.png",
        "https://cdn.iconscout.com/icon/free/png-256/round-circle-shape-design-disc-3-15991.png",
        "https://cdn.iconscout.com/icon/free/png-256/architechture-square-shape-design-3-15934.png",
        "https://cdn.iconscout.com/icon/free/png-256/bank-mony-income-black-economy-finance-3-15963.png",
        "https://cdn.iconscout.com/icon/free/png-256/medical-bottle-plus-hospital-medicine-tfreatment-3-15959.png",
        "https://cdn.iconscout.com/icon/free/png-256/pin-collobrate-attach-joint-3-15943.png",
        "https://cdn.iconscout.com/icon/free/png-256/tant-tracking-camp-night-village-3-15941.png",
        "https://cdn.iconscout.com/icon/free/png-512/chat-text-message-chatting-talk-notification-6-15933.png",
        "https://cdn.iconscout.com/icon/free/png-256/arrow-direction-right-way-forward-next-11-15989.png",
        "https://cdn.iconscout.com/icon/free/png-256/pin-mark-attach-joint-point-stick-3-15940.png",
        "https://cdn.iconscout.com/icon/free/png-256/star-bookmark-favorite-shape-rank-3-16006.png",
        "https://cdn.iconscout.com/icon/free/png-256/education-magnet-attract-distract-attach-iron-3-16011.png",
        "https://cdn.iconscout.com/icon/free/png-256/grid-application-menu-artboard-layer-3-15967.png",
        "https://cdn.iconscout.com/icon/free/png-256/eye-mission-vision-view-internet-web-search-7035.png",
        "https://cdn.iconscout.com/icon/free/png-512/cloud-data-optimization-performance-report-pie-chart-statics-analysis-7007.png",
        "https://cdn.iconscout.com/icon/free/png-256/creative-marketing-idea-statics-pie-seo-optimization-7019.png",
        "https://cdn.iconscout.com/icon/free/png-256/market-research-glass-tube-statics-report-chart-seo-7012.png",
        "https://cdn.iconscout.com/icon/free/png-256/webcam-web-camera-videocall-interface-ui-1-13276.png",
        "https://cdn.iconscout.com/icon/free/png-512/ui-bright-brightness-sun-light-contrast-setting-13237.png",
        "https://cdn.iconscout.com/icon/free/png-512/tree-outline-stroke-environment-interface-ui-13233.png",
        "https://cdn.iconscout.com/icon/free/png-256/tune-music-melody-beat-interface-ui-13235.png",
        "https://cdn.iconscout.com/icon/free/png-512/antena-wifi-signal-waves-wireless-interface-ui-1-13334.png",
        "https://cdn.iconscout.com/icon/free/png-256/data-science-filter-conversion-funnel-seo-traffic-1-5186.png",
        "https://cdn.iconscout.com/icon/free/png-256/folder-file-explorer-document-office-package-storage-2502.png",
        "https://cdn.iconscout.com/icon/free/png-512/performance-measure-position-valuation-number-statics-analysis-2-24710.png",
        "https://cdn.iconscout.com/icon/free/png-256/love-romantic-valentine-day-rose-lotus-flower-9-24194.png",
        "https://cdn.iconscout.com/icon/free/png-256/law-crime-jurist-dress-graduate-hat-12-28944.png",
        "https://cdn.iconscout.com/icon/free/png-256/sales-statics-analysis-report-strategy-board-performance-2-24718.png",
        "https://cdn.iconscout.com/icon/free/png-256/weather-rain-season-cloud-rainy-1-28907.png",
        "https://cdn.iconscout.com/icon/free/png-256/android-menu-grid-app-view-application-outline-2501.png",
        "https://cdn.iconscout.com/icon/free/png-256/love-romantic-valentine-day-date-dinner-snacks-9-24190.png"
    ];

    const iconPanel = createIconPanel(iconUrls);

    const appendIconButtons = (uploadPanel) => {
        if (!uploadPanel.querySelector('.custom-icon-button')) {
            uploadPanel.appendChild(iconPanel);
        }
    };

    const removeIconButtons = (uploadPanel) => {
        const iconButton = uploadPanel.querySelector('.custom-icon-panel');
        if (iconButton) {
            uploadPanel.removeChild(iconButton);
        }
    };

    const observeIconPanel = (mutationsList) => {
        for (let mutation of mutationsList) {
            // 添加
            if (mutation.addedNodes) {
                mutation.addedNodes.forEach((node) => {
                    if (node.nodeType === Node.ELEMENT_NODE) {
                        if (node.querySelector(`${iconPanelSelector} ${uploadPictureSelector}`)) {
                            appendIconButtons(node.parentNode);
                        }
                    }
                });
            }
            // 删除
            if (mutation.removedNodes) {
                mutation.removedNodes.forEach((node) => {
                    if (node.nodeType === Node.ELEMENT_NODE) {
                        if (node.querySelector(`${uploadPictureSelector}`)) {
                            removeIconButtons(mutation.target);
                        }
                    }
                });
            }
        }
    };

    const observer = new MutationObserver(observeIconPanel);
    observer.observe(document.body, {childList: true, subtree: true});

    const style = document.createElement('style');
    style.textContent = `
.custom-icon-button:hover {
  background: rgba(55, 53, 47, 0.08);
}

body.dark .custom-icon-button:hover {
  background: rgba(255, 255, 255, 0.055) !important;
}


.custom-icon-button {
  user-select: none;
  transition: background 20ms ease-in 0s;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  width: 32px;
  height: 32px;
  font-size: 24px;
  transition: opacity 0.2s ease;
}

.custom-icon-button:active {
  opacity: 0.2
}

.custom-icon-button > img {
  width: 24px;
  height: 24px;
}

.custom-icon-panel {
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  background: transparent;
  padding-left: 12px;
  padding-right: 12px;
}`
    document.head.appendChild(style);
})();